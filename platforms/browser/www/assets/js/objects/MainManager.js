/* ------------------------ CONSTRUCTEUR ------------------------ */
var MainManager = function() {
  // On défini les data
  data = {
    lang: null,
    story: null,
    version: null,
  };

  // On déclare les tmer
  loadingStep1 = null;
  loadingStep2 = null;
  loadingStep3 = null;
  loadingStep4 = null;
  loadingStep5 = null;
  loadingStep6 = null;
  loadingStep7 = null;

  // On init les événements
  this.initEvents();
};

/* ------------------------ FONCTIONS D'INITITIALISATION ------------------------ */
/* initEvents
 *
 * Cette fonction permet d'initialiser les événements nécessaires au bon fonctionnement de l'objet
 *
 */
MainManager.prototype.initEvents = function() {
  // On initialise la hauteur de la webapp
  this.initViewportHeigh();

  // Au resize, on relance l'initialisation de la hauteur de la webapp
  window.addEventListener('resize', () => {
    this.initViewportHeigh();
  });

  // On affiche le contenu de la page d'accueil (login)
  this.showHome();

};

/* initViewportHeigh
 *
 * Cette fonction permet d'initialiser la hauteur du viewport de la webapp
 *
 */
MainManager.prototype.initViewportHeigh = function() {
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
};

/* initHomeEvents
 *
 * Cette fonction permet d'initialiser les événements de la page d'accueil
 *
 */
MainManager.prototype.initHomeEvents = function() {
  // On écoute le clic sur le bouton de login
  $("#btn-submit-login").on("click", this.handleClickOnBtnSubmitLogin.bind(this));
};

/* initLangChoiceEvents
 *
 * Cette fonction permet d'initialiser les événements de la page de sélection de langue
 *
 */
MainManager.prototype.initLangChoiceEvents = function() {
  // On écoute le clic sur le bouton retour
  $("#btn-back").on("click", this.handleClickOnBtnBackOnLangChoice.bind(this));

  // On écoute le clic sur le bouton ENGLISH
  $(".item-lang").on("click", this.handleClickOnItemLang.bind(this));
};

/* initStoryChoiceEvents
 *
 * Cette fonction permet d'initialiser les événements de la page de sélection d'histoires
 *
 */
MainManager.prototype.initStoryChoiceEvents = function() {
  // On arrête tous les timer
  this.clearLoadingTimeout();

  // On écoute le clic sur le bouton retour
  $("#btn-back").on("click", this.handleClickOnBtnBackOnStoryChoice.bind(this));

  // On initialise le slider des histoires
  $(".list-stories").slick({
    arrows: true,
    prevArrow: $("#btn-prev-story"),
    nextArrow: $("#btn-next-story"),
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });

  // On écoute le clic sur une histoire
  $(".list-stories .item-story").on("click", this.handleClickOnItemStory.bind(this));
};

/* initLoadingEvents
 *
 * Cette fonction permet d'initialiser les événements de la page de chargement
 *
 */
MainManager.prototype.initLoadingEvents = function() {
  // On écoute le clic sur le bouton retour
  $("#btn-back").on("click", this.handleClickOnBtnBackOnLoading.bind(this));

  // On permet le clic sur le texte
  $("#screen-loading").on("click", this.handleClickOnTextLoading.bind(this));

  // On arrête tous les timer
  this.clearLoadingTimeout();

  // On masque le message informatif
  loadingStep1 = setTimeout(function() {
    $(".text-loading").addClass("remove");
  }, 6000);

  loadingStep2 = setTimeout(function() {
    $(".text-loading").hide();

    // On lance le compteur
    this.showTimer();
  }.bind(this), 6600);
};

/* initPlayerEvents
 *
 * Cette fonction permet d'initialiser les événements de la page de lecture audio
 *
 */
MainManager.prototype.initPlayerEvents = function() {
  // On écoute le clic sur le bouton retour
  $("#btn-back").on("click", this.handleClickOnBtnBackOnPlayer.bind(this));

  // On défini le html de l'audio
  const htmlAudio = "<audio id='audio-file' src='assets/sounds/"+data.lang+"_"+data.story+"_"+data.version+".wav' preload='auto' controls></audio>";

  // On affiche le player audio
  $(".player-audio").html(htmlAudio);

  // On initialise le player audio
  $(".player-audio audio").audioPlayer();

  // On défini le html de l'image de l'histoire
  const htmlStory = "<img src='assets/images/stories/"+data.lang+"/"+data.story+".jpg' alt='' border='0' />";

  // On affiche l'image de l'histoire
  $(".story-selected").html(htmlStory);

  // On détecte la fin du fichier audio
  $("#audio-file").on("ended", function() {
     // On affiche l'écran de choix d'histoire
     this.showStoryChoice(data.lang);
  }.bind(this));

};


/* ------------------------ AFFICHAGE DES PAGES ---------------------------- */
/* showHome
 *
 * Cette fonction permet de charger le contenu de home.html
 *
 */
MainManager.prototype.showHome = function() {

  this.initHomeEvents();

  // this.resetWebapp();

  // $("#app").load("views/home.html", function(){

  //   // On initialise les événements
  //   this.initHomeEvents();

  // }.bind(this));

};

/* showLangChoice
 *
 * Cette fonction permet de charger le contenu de lang_choice.html
 *
 */
MainManager.prototype.showLangChoice = function() {

  this.resetWebapp();

  $("#app").load("views/lang_choice.html", function(){

    // On initialise les événements
    this.initLangChoiceEvents();

  }.bind(this));

};

/* showStoryChoice
 *
 * Cette fonction permet de charger le contenu de story_choice.html selon la langue sélectionnée
 *
 */
MainManager.prototype.showStoryChoice = function(curLang) {

  this.resetWebapp();

  $("#app").load("views/"+curLang+"/story_choice.html", function(){

    // On initialise les événements
    this.initStoryChoiceEvents();

  }.bind(this));

};

/* showLoading
 *
 * Cette fonction permet de charger le contenu de loading.html
 *
 */
MainManager.prototype.showLoading = function(curLang) {

  this.resetWebapp();

  $("#app").load("views/"+curLang+"/loading.html", function(){

    // On initialise les événements
    this.initLoadingEvents();

  }.bind(this));

};

/* showTimer
 *
 * Cette fonction permet de charger le timer avant affichage du player
 *
 */
MainManager.prototype.showTimer = function() {
  loadingStep3 = setTimeout(function() {
    // On affiche le 3
    $(".count .num-3").removeClass("inactive");
    $(".count .num-3").addClass("active");
  }, 200);

  loadingStep4 = setTimeout(function() {
    // On masque le 3
    $(".count .num-3").removeClass("active");
    $(".count .num-3").addClass("inactive");
    // On affiche le 2
    $(".count .num-2").removeClass("inactive");
    $(".count .num-2").addClass("active");
  }, 1200);

  loadingStep5 = setTimeout(function() {
      // On masque le 2
    $(".count .num-2").removeClass("active");
    $(".count .num-2").addClass("inactive");
    // On affiche le 1
    $(".count .num-1").removeClass("inactive");
    $(".count .num-1").addClass("active");
  }, 2200);

  loadingStep6 = setTimeout(function() {
    // On masque le 1
    $(".count .num-1").removeClass("active");
    $(".count .num-1").addClass("inactive");
  }, 3200);

  loadingStep7 = setTimeout(function() {

    // On affiche la page suivante
    this.showPlayer();

  }.bind(this), 4000);
};

/* showPlayer
 *
 * Cette fonction permet de charger le contenu de player.html
 *
 */
MainManager.prototype.showPlayer = function(theme) {

  this.resetWebapp();

  $("#app").load("views/player.html", function(){

    if (theme === "soon") {
      // On affiche le message temporaire
      $("#screen-player").addClass("soon");

      // On permet le clic sur le bouton retour
      $("#btn-back").on("click", this.handleClickOnBtnBackOnPlayer.bind(this));

    } else {

      // On initialise les événements
      this.initPlayerEvents();

    }

  }.bind(this));

};

/* resetWebapp
 *
 * Cette fonction permet de vider ce qui est contenu dans #app
 *
 */
MainManager.prototype.resetWebapp = function() {
  $("#app").html("");
};

/* ------------------------ BOUTON RETOUR ------------------------ */
/* handleClickOnBtnBackOnLangChoice
 *
 * Cette fonction permet de gérer le clic sur le bouton retour à partir de l'écran de sélection de langue
 *
 */
MainManager.prototype.handleClickOnBtnBackOnLangChoice = function() {
  // On affiche le contenu de la page d'accueil (login)
  this.showHome();
};

/* handleClickOnBtnBackOnStoryChoice
 *
 * Cette fonction permet de gérer le clic sur le bouton retour à partir de l'écran de sélection d'histoire
 *
 */
MainManager.prototype.handleClickOnBtnBackOnStoryChoice = function() {
  // On affiche le contenu de la page de sélection de langue
  this.showLangChoice();
};

/* handleClickOnBtnBackOnLoading
 *
 * Cette fonction permet de gérer le clic sur le bouton retour à partir de l'écran de chargement
 *
 */
MainManager.prototype.handleClickOnBtnBackOnLoading = function() {
  // On récupère la langue en cours
  const curLang = data.lang;

  // On affiche le contenu de la page de sélection d'histoire
  this.showStoryChoice(curLang);
};

/* handleClickOnBtnBackOnPlayer
 *
 * Cette fonction permet de gérer le clic sur le bouton retour à partir de l'écran de lecture audio
 *
 */
MainManager.prototype.handleClickOnBtnBackOnPlayer = function() {
  // On récupère la langue en cours
  const curLang = data.lang;

  // On affiche le contenu de la page de sélection d'histoire
  this.showStoryChoice(curLang);
};


/* ------------------------ FORMULAIRE LOGIN ----------------------------- */
/* handleClickOnBtnSubmitLogin
 *
 * Cette fonction permet de gérer le clic sur le bouton de soumission du formulaire de login
 *
 */
MainManager.prototype.handleClickOnBtnSubmitLogin = function(e) {

  // On annule la soumission du formulaire
  e.preventDefault();

  // On récupère le mot de passe renseigné
  const curPassword = $("input[name=password]").val();

  if (curPassword === "#tos20" || curPassword === "#TOS20") {

    // On masque le message d'erreur
    $(".login-form .error").hide();

    // On charge le contenu de la page de sélection de langue
    this.showLangChoice();

  } else {

    // On affiche le message d'erreur
    $(".login-form .error").show();

  }
};

/* ------------------------ SELECTION LANGUE ----------------------------- */
/* handleClickOnItemLang
 *
 * Cette fonction permet de gérer le clic sur une langue
 *
 */
MainManager.prototype.handleClickOnItemLang = function(e) {

  if ($(e.currentTarget).attr("class").indexOf("soon") < 0) {
    // On récuupère le code de la langue sélectionnée
    const curLang = $(e.currentTarget).attr("data-lang");

    // On met à jour les données
    data = {
      lang: curLang,
      story: null,
      version: null
    };

    // On affiche la page suivante
    this.showStoryChoice(curLang);
  }

}

/* ------------------------ SELECTION HISTOIRE ----------------------------- */
/* handleClickOnItemStory
 *
 * Cette fonction permet de gérer le clic sur une histoire
 *
 */
MainManager.prototype.handleClickOnItemStory = function(e) {

  // On récupère l'histoire sélectionnée
  const curStory = $(e.currentTarget).attr("data-story");

  // On retire la class active de tous les items
  $(".list-stories .item-story").removeClass("selected");

  // On ajoute la class active sur l'item cliqué
  $(e.currentTarget).addClass("selected");

  // On met à jour le conteneur des histoires
  $(".list-stories").removeClass("active-item");
  $(".list-stories").addClass("active-item");

  // On affiche les boutons de version - FONCTIONNALITÉ RETIRÉE LE 15.07.2020
  // $(".line-buttons-bottom").addClass("active");

  // On met à jour les données
  data = {
    lang: data.lang,
    story: curStory,
    version: "long" // UPDATE DU 15.07.2020 - On utilise temporairement uniquement les versions longues des histoires
  };

  // On écoute le clic sur les boutons de version - FONCTIONNALITÉ RETIRÉE LE 15.07.2020
  // $(".line-buttons-bottom .btn-version").on("click", this.handleClickOnBtnVersion.bind(this));

  // On vérifie si le fichier audio existe et on affiche la page correspondante - FONCTIONNALITÉ AJOUTÉE LE 15.07.2020
  this.checkAudioFileAndShowNextPage(data);

};

/* handleClickOnBtnVersion
 *
 * Cette fonction permet de gérer le clic sur un bouton de version
 *
 */
MainManager.prototype.handleClickOnBtnVersion = function(e) {
  // On récupère la version sélectionnée
  const curVersion = $(e.currentTarget).attr("data-version");

  // On met à jour les données
  data = {
    lang: data.lang,
    story: data.story,
    version: curVersion
  };

  // On vérifie si le fichier audio existe et on affiche la page correspondante
  this.checkAudioFileAndShowNextPage(data);

};

/* checkAudioFileAndShowNextPage
 *
 * Cette fonction permet de vérifier si le fichier audio existe et de passer à l'écran suivant
 *
 */
MainManager.prototype.checkAudioFileAndShowNextPage = function(data) {
  $.ajax({
    url: window.location.href+"assets/sounds/"+data.lang+"_"+data.story+"_"+data.version+".wav",
    type:'HEAD',
    error: function() {

      // On affiche la page player en version "soon"
      this.showPlayer("soon");

    }.bind(this),
    success: function() {

      // On affiche la page suivante
      this.showLoading(data.lang);

    }.bind(this)
  });
}

/* ------------------------ LOADING ----------------------------- */
/* handleClickOnTextLoading
 *
 * Cette fonction permet de gérer le clic sur le texte de l'écran de loading
 *
 */
MainManager.prototype.handleClickOnTextLoading = function(e) {
  // On arrête tous les timer
  this.clearLoadingTimeout();

  // On masque le message informatif
  loadingStep1 = setTimeout(function() {
    $(".text-loading").addClass("remove");
  }, 50);

  loadingStep2 = setTimeout(function() {
    $(".text-loading").hide();

    // On lance le compteur
    this.showTimer();

  }.bind(this), 100);
};

/* clearLoadingTimeout
 *
 * Cette fonction permet d'arrêter tous les timer de la page loading
 *
 */
MainManager.prototype.clearLoadingTimeout = function(e) {
  clearTimeout(loadingStep1);
  clearTimeout(loadingStep2);
  clearTimeout(loadingStep3);
  clearTimeout(loadingStep4);
  clearTimeout(loadingStep5);
  clearTimeout(loadingStep6);
  clearTimeout(loadingStep7);
};